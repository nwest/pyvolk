import pyvolk
import numpy as np
from timeit import timeit

a = np.random.randn(1000)
b = np.random.randn(1000)

print (timeit(lambda: a*b, number = 100000))
print (timeit(lambda: pyvolk.multiply(a,b), number = 100000))
