//
// Created by root on 10/6/17.
//

#ifndef PYVOLK_CONVERT_H
#define PYVOLK_CONVERT_H

#include <volk/volk.h>
#include <Python.h>
#include <numpy/ndarrayobject.h>


// TODO: we should wrap up a lot of the boiler plate shit to some utility functions where possible
// That might wind up just being type checking and safety code

static char convert_to_64f_doc[] = "convert_to_64f(vec)"
        " convert elements of vec to 64f";

static PyObject *
convert_to_64f_wrapper(PyObject *self, PyObject *args)
{
    PyObject *inObj = NULL;
    PyArg_ParseTuple(args, "O", &inObj);
    PyArrayObject *aArray = (PyArrayObject *) inObj;

    PyArray_Descr *a_dtype = PyArray_DTYPE(aArray);
    int ndims = PyArray_NDIM(aArray);
    npy_intp *arr_shape = PyArray_SHAPE(aArray);

    unsigned int num_points = 0;
    for (unsigned int dim_index = 0; dim_index < ndims; ++dim_index) {
        num_points += arr_shape[dim_index];
    }
    PyObject *outputArray = PyArray_SimpleNew(ndims, arr_shape, NPY_FLOAT64);
    if (a_dtype->type_num == NPY_FLOAT32) {
        volk_32f_convert_64f(PyArray_DATA((PyArrayObject*)outputArray),
                          PyArray_DATA(aArray),
                          num_points);
    }
    return outputArray;
}


#endif //PYVOLK_CONVERT_H
