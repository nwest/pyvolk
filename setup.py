from distutils.core import setup, Extension
import numpy
import glob

pyvolk_module = Extension('pyvolk',
            sources = glob.glob('*.c'),
            libraries = ['volk'],
            library_dirs = ['/Users/nathan/anaconda2/lib'],
            include_dirs = ['/Users/nathan/anaconda2/include', numpy.get_include()])

setup(name = 'pyvolkname',
    version = '0.0',
    description = 'Python bindings for volk',
    ext_modules = [pyvolk_module],
    author='Nathan West')
