//
// Created by Nathan West on 1/31/18.
//

#ifndef PYVOLK_MULTIPLY_H
#define PYVOLK_MULTIPLY_H

#include "type_utils.h"
#include <volk/volk.h>
#include <Python.h>
#include <numpy/ndarrayobject.h>


PyArrayObject * multiply_equal_length_arrays(PyArrayObject *aArray, PyArrayObject *bArray)
{
    PyArrayObject *outputArray = NULL;
    PyArray_Descr *a_dtype = PyArray_DTYPE(aArray);
    PyArray_Descr *b_dtype = PyArray_DTYPE(bArray);

    unsigned int num_points = 0;
    int ndims = PyArray_NDIM(aArray);
    npy_intp *shape = PyArray_SHAPE(aArray);
    for (int dim_index = 0; dim_index < ndims; ++dim_index) {
        num_points += shape[dim_index];
    }

    switch (get_dtype_permutation(a_dtype, b_dtype)) {
        case F32_F32:
            outputArray = (PyArrayObject*)PyArray_SimpleNew(ndims, shape, NPY_FLOAT32);
            volk_32f_x2_multiply_32f(PyArray_DATA(outputArray), PyArray_DATA(aArray), PyArray_DATA(bArray), num_points);
            break;
        case CF32_CF32:
            outputArray = (PyArrayObject*)PyArray_SimpleNew(ndims, shape, NPY_COMPLEX64);
            volk_32fc_x2_multiply_32fc(PyArray_DATA(outputArray), PyArray_DATA(aArray), PyArray_DATA(bArray), num_points);
            break;
        case F64_F64:
            outputArray = (PyArrayObject*)PyArray_SimpleNew(ndims, shape, NPY_FLOAT64);
            volk_64f_x2_multiply_64f(PyArray_DATA(outputArray), PyArray_DATA(aArray), PyArray_DATA(bArray), num_points);
            break;
        case CF32_F32:
            outputArray = (PyArrayObject*)PyArray_SimpleNew(ndims, shape, NPY_COMPLEX64);
            volk_32fc_32f_multiply_32fc(PyArray_DATA(outputArray), PyArray_DATA(bArray), PyArray_DATA(aArray), num_points);
            break;
        case F32_CF32:
            outputArray = (PyArrayObject*)PyArray_SimpleNew(ndims, shape, NPY_COMPLEX64);
            volk_32fc_32f_multiply_32fc(PyArray_DATA(outputArray), PyArray_DATA(aArray), PyArray_DATA(bArray), num_points);
            break;
        case F32_F64:
            outputArray = (PyArrayObject*)PyArray_SimpleNew(ndims, shape, NPY_FLOAT64);
            volk_32f_64f_multiply_64f(PyArray_DATA(outputArray), PyArray_DATA(aArray), PyArray_DATA(bArray), num_points);
        case F64_F32:
            outputArray = (PyArrayObject*)PyArray_SimpleNew(ndims, shape, NPY_FLOAT64);
            volk_32f_64f_multiply_64f(PyArray_DATA(outputArray), PyArray_DATA(bArray), PyArray_DATA(aArray), num_points);
        case UNKNOWN:
            PyErr_SetString(PyExc_TypeError, "multiply does not support the given types");
            outputArray = NULL;
    }

    return outputArray;
}

static char multiply_doc[] = "multiply(vec)"
        " (element-by-element) multiply two arrays";

static PyObject *
multiply_wrapper(PyObject *self, PyObject *args)
{
    PyObject *arrA = NULL;
    PyObject *arrB = NULL;
    PyArg_ParseTuple(args, "OO", &arrA, &arrB);
    PyArrayObject *aArray = (PyArrayObject *) arrA;
    PyArrayObject *bArray = (PyArrayObject *) arrB;

    PyObject *outputArray=NULL;

    bool same_shape = verify_same_shape(aArray, bArray);
    if (same_shape == true) {
        outputArray = (PyObject*) multiply_equal_length_arrays(aArray, bArray);
    }

    return outputArray;
}

#endif //PYVOLK_MULTIPLY_H
