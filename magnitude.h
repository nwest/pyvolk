//
// Created by nathan on 10/6/17.
//

#ifndef PYVOLK_MAGNITUDE_H
#define PYVOLK_MAGNITUDE_H

#include <volk/volk.h>
#include <Python.h>
#include <numpy/ndarrayobject.h>


static char magnitude_doc[] = "magnitude(vec)"
" compute the magnitude of an array";

static PyObject *
magnitude_wrapper(PyObject *self, PyObject *args)
{
    PyObject *inObj = NULL;
    PyArg_ParseTuple(args, "O", &inObj);
    PyArrayObject *aArray = (PyArrayObject *) inObj;

    PyArray_Descr *a_dtype = PyArray_DTYPE(aArray);
    int ndims = PyArray_NDIM(aArray);
    npy_intp *arr_shape = PyArray_SHAPE(aArray);

    unsigned int num_points = 0;
    for (unsigned int dim_index = 0; dim_index < ndims; ++dim_index) {
        num_points += arr_shape[dim_index];
    }
    PyObject *outputArray = PyArray_SimpleNew(ndims, arr_shape, NPY_FLOAT32);
    if (a_dtype->type_num == NPY_COMPLEX64) {
        volk_32fc_magnitude_32f(PyArray_DATA((PyArrayObject*)outputArray),
                                PyArray_DATA(aArray),
                                num_points);
    }
    return outputArray;
}

static PyObject *
magnitude_squared_wrapper(PyObject *self, PyObject *args)
{
    PyObject *inObj = NULL;
    PyArg_ParseTuple(args, "O", &inObj);
    PyArrayObject *aArray = (PyArrayObject *) inObj;

    PyArray_Descr *a_dtype = PyArray_DTYPE(aArray);
    int ndims = PyArray_NDIM(aArray);
    npy_intp *arr_shape = PyArray_SHAPE(aArray);

    unsigned int num_points = 0;
    for (unsigned int dim_index = 0; dim_index < ndims; ++dim_index) {
        num_points += arr_shape[dim_index];
    }
    PyObject *outputArray = PyArray_SimpleNew(ndims, arr_shape, NPY_FLOAT32);
    if (a_dtype->type_num == NPY_COMPLEX64) {
        volk_32fc_magnitude_squared_32f(PyArray_DATA((PyArrayObject*)outputArray),
                                PyArray_DATA(aArray),
                                num_points);
    }
    return outputArray;
}

#endif //PYVOLK_MAGNITUDE_H
