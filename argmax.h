//
// Created by Nathan West on 1/24/18.
//

#ifndef PYVOLK_ARGMAX_H
#define PYVOLK_ARGMAX_H

#include <stdint.h>
#include <volk/volk.h>
#include <Python.h>
#include <numpy/ndarrayobject.h>

static char argmax_16u_doc[] = "argmax_16u(vec)"
    " find the index of the maximum value in the vector (calls volk_32*_index_max_16u)";

static PyObject *
argmax_16u_wrapper(PyObject *self, PyObject *args)
{
    PyObject *inObj = NULL;
    PyArg_ParseTuple(args, "O", &inObj);
    PyArrayObject *aArray = (PyArrayObject *) inObj;

    PyArray_Descr *a_dtype = PyArray_DTYPE(aArray);
    int ndims = PyArray_NDIM(aArray);
    npy_intp *arr_shape = PyArray_SHAPE(aArray);

    unsigned int num_points = 0;
    for (unsigned int dim_index = 0; dim_index < ndims; ++dim_index) {
        num_points += arr_shape[dim_index];
    }
    uint16_t resultValue;
    if (a_dtype->type_num == NPY_COMPLEX64) {
        volk_32fc_index_max_16u_manual(&resultValue,
                             PyArray_DATA(aArray),
                             num_points, "generic");
    }
#if PY_MAJOR_VERSION == 2
    return PyInt_FromLong((long)resultValue);
#elif PY_MAJOR_VERSION == 3
    return PyLong_FromLong((long)resultValue);
#endif
}

#endif //PYVOLK_ARGMAX_H
