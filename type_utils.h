//
// Created by Nathan West on 2/1/18.
//

#ifndef PYVOLK_TYPE_UTILS_H
#define PYVOLK_TYPE_UTILS_H

#include <Python.h>
#include <numpy/ndarrayobject.h>

bool verify_same_shape(PyArrayObject *a, PyArrayObject *b)
{
    int a_ndims = PyArray_NDIM(a);
    int b_ndims = PyArray_NDIM(b);

    npy_intp *a_arr_shape = PyArray_SHAPE(a);
    npy_intp *b_arr_shape = PyArray_SHAPE(b);

    bool same_shape = true;

    if (a_ndims == b_ndims) {
        for (int dim=0; dim < a_ndims; ++dim) {
            if (a_arr_shape[dim] != b_arr_shape[dim]) {
                char err[4096];
                snprintf(err, 4096, "Arrays must be same length, but index %i does not match (%li != %li)", dim, a_arr_shape[dim], b_arr_shape[dim]);
                PyErr_SetString(PyExc_IndexError, err);
                same_shape = false;
            }
        }
    } else {
        char err[4096];
        snprintf(err, 4096, "Arrays must be same length, but have different dimensions (%i = %i)", a_ndims, b_ndims);
        PyErr_SetString(PyExc_IndexError, err);
        same_shape = false;
    }
    return same_shape;
}

typedef enum {
    F32_F32,
    F64_F64,
    F32_F64,
    F64_F32,
    CF32_CF32,
    CF32_F32,
    F32_CF32,
    UNKNOWN
} TWO2DTYPE_PERMUTATION;

TWO2DTYPE_PERMUTATION get_dtype_permutation(PyArray_Descr *a, PyArray_Descr *b)
{
    TWO2DTYPE_PERMUTATION permute_type;
    if (a->type_num == b->type_num) { // probably a very common case, they are the same
        switch (a->type_num) {
            case NPY_FLOAT32:
                permute_type = F32_F32;
                break;
            case NPY_FLOAT64:
                permute_type = F64_F64;
                break;
            case NPY_COMPLEX64:
                permute_type = CF32_CF32;
                break;
            default:
                permute_type = UNKNOWN;
        }
    } else if (a->type_num == NPY_FLOAT32) { // first is float
        switch (b->type_num) {
            case NPY_COMPLEX64:
                permute_type = F32_CF32;
                break;
            case NPY_FLOAT64:
                permute_type = F32_F64;
                break;
            default:
                permute_type = UNKNOWN;
        }
    } else if (a->type_num == NPY_COMPLEX64) { // first is complex
        switch (b->type_num) {
            case NPY_FLOAT32:
                permute_type = CF32_F32;
                break;
            default:
                permute_type = UNKNOWN;
        }
    } else if (a->type_num == NPY_FLOAT64) { // first is double
        switch (b->type_num) {
            case NPY_FLOAT32:
                permute_type = F64_F32;
                break;
            default:
                permute_type = UNKNOWN;
        }
    }
    return permute_type;
}

#endif //PYVOLK_TYPE_UTILS_H
