//
// Created by nathan on 10/6/17.
//

#ifndef PYVOLK_LOG_H
#define PYVOLK_LOG_H

#include <volk/volk.h>
#include <Python.h>
#include <numpy/ndarrayobject.h>


// TODO: we should wrap up a lot of the boiler plate shit to some utility functions where possible
// That might wind up just being type checking and safety code

static char log2_doc[] = "log2(vec)"
" compute the log2 of elements in an array";

static PyObject *
log2_wrapper(PyObject *self, PyObject *args)
{
    PyObject *inObj = NULL;
    PyArg_ParseTuple(args, "O", &inObj);
    PyArrayObject *aArray = (PyArrayObject *) inObj;

    PyArray_Descr *a_dtype = PyArray_DTYPE(aArray);
    int ndims = PyArray_NDIM(aArray);
    npy_intp *arr_shape = PyArray_SHAPE(aArray);

    unsigned int num_points = 0;
    for (unsigned int dim_index = 0; dim_index < ndims; ++dim_index) {
        num_points += arr_shape[dim_index];
    }
    PyObject *outputArray = PyArray_SimpleNew(ndims, arr_shape, NPY_FLOAT32);
    if (a_dtype->type_num == NPY_FLOAT32) {
        volk_32f_log2_32f(PyArray_DATA((PyArrayObject*)outputArray),
                                PyArray_DATA(aArray),
                                num_points);
    }
    return outputArray;
}

static char log10_doc[] = "log10(vec)"
        " compute the log10 of elements in an array";

static PyObject *
log10_wrapper(PyObject *self, PyObject *args)
{
    PyObject *inObj = NULL;
    PyArg_ParseTuple(args, "O", &inObj);
    PyArrayObject *aArray = (PyArrayObject *) inObj;

    PyArray_Descr *a_dtype = PyArray_DTYPE(aArray);
    int ndims = PyArray_NDIM(aArray);
    npy_intp *arr_shape = PyArray_SHAPE(aArray);

    unsigned int num_points = 0;
    for (unsigned int dim_index = 0; dim_index < ndims; ++dim_index) {
        num_points += arr_shape[dim_index];
    }
    PyObject *outputArray = PyArray_SimpleNew(ndims, arr_shape, NPY_FLOAT32);
    if (a_dtype->type_num == NPY_FLOAT32) {
        volk_32f_log2_32f(PyArray_DATA((PyArrayObject*)outputArray),
                          PyArray_DATA(aArray),
                          num_points);
        volk_32f_s32f_multiply_32f(PyArray_DATA((PyArrayObject*)outputArray),
                                   PyArray_DATA((PyArrayObject*)outputArray),
                                   0.3010299956639812f,
                                   num_points);
    }
    return outputArray;
}

#endif