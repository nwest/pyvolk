//
// Created by Nathan West on 1/31/18.
//

#ifndef PYVOLK_rotator_H
#define PYVOLK_rotator_H

#include "type_utils.h"
#include <volk/volk.h>
#include <Python.h>
#include <numpy/ndarrayobject.h>


static char rotator_doc[] = "rotator(vec, phase_increment, initial_phase)"
        " multiply a complex array by an oscillator spinning at phase_increment with initial_phase";

static PyObject *
rotator_wrapper(PyObject *self, PyObject *args)
{
    PyObject *ainputArray = NULL;
    PyObject *initialPhaseScalar = NULL;
    PyObject *frequencyScalar = NULL;
    PyArg_ParseTuple(args, "OOO", &ainputArray, &frequencyScalar, &initialPhaseScalar);

    lv_32fc_t frequency;
    lv_32fc_t phase;
    Py_complex f_cplx;
    Py_complex ph_cplx;
    PyArrayObject *aArray = (PyArrayObject *) ainputArray;

    if (PyArray_CheckScalar(frequencyScalar)) {
        PyArray_ScalarAsCtype(frequencyScalar, &frequency);
    } else if PyComplex_Check(frequencyScalar) {
        f_cplx = PyComplex_AsCComplex(frequencyScalar);
        frequency = lv_cmake(f_cplx.real, f_cplx.imag);
    } else {
        PyErr_BadArgument();
        return NULL;
    }
    if (PyArray_CheckScalar(initialPhaseScalar)) {
        PyArray_ScalarAsCtype(initialPhaseScalar, &phase);
    } else if PyComplex_Check(frequencyScalar) {
        ph_cplx = PyComplex_AsCComplex(initialPhaseScalar);
        phase = lv_cmake(ph_cplx.real, ph_cplx.imag);
    } else {
        PyErr_BadArgument();
        return NULL;
    }

    int ndims = PyArray_NDIM(aArray);
    npy_intp *shape = PyArray_SHAPE(aArray);
    PyObject *outputArray=NULL;
    outputArray = PyArray_SimpleNew(ndims, shape, NPY_COMPLEX64);

    volk_32fc_s32fc_x2_rotator_32fc(PyArray_DATA(outputArray),
    PyArray_DATA(aArray), frequency, &phase, shape[0]);

    return outputArray;
}

#endif //PYVOLK_rotator_H
