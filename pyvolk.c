//
// Created by nathan on 10/6/17.
//

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/ndarrayobject.h>
#include <numpy/ndarraytypes.h>
#include <methodobject.h>

#include "magnitude.h"
#include "log.h"
#include "convert.h"
#include "argmax.h"
#include "multiply.h"
#include "pv_add.h"
#include "rotator.h"

static PyMethodDef mymethods[] = {
        {"magnitude", magnitude_wrapper, METH_VARARGS, magnitude_doc},
        {"magnitude_squared", magnitude_squared_wrapper, METH_VARARGS, magnitude_doc},
        {"log2", log2_wrapper, METH_VARARGS, log2_doc},
        {"log10", log10_wrapper, METH_VARARGS, log10_doc},
        {"to_64f", convert_to_64f_wrapper, METH_VARARGS, convert_to_64f_doc},
        {"argmax_16u", argmax_16u_wrapper, METH_VARARGS, argmax_16u_doc},
        {"multiply", multiply_wrapper, METH_VARARGS, multiply_doc},
        {"add", add_wrapper, METH_VARARGS, add_doc},
        {"rotator", rotator_wrapper, METH_VARARGS, rotator_doc},
        {NULL, NULL, 0, NULL} /* Sentinel */
};

#if PY_MAJOR_VERSION == 2
PyMODINIT_FUNC
initpyvolk(void)
{
    PyObject *m = Py_InitModule("pyvolk", mymethods);
    import_array();
}

#elif PY_MAJOR_VERSION == 3
static struct PyModuleDef pyvolkmodule = {
    PyModuleDef_HEAD_INIT,
    "pyvolk",
    "a pythonic wrapper around VOLK",
    -1,
    mymethods
};

PyMODINIT_FUNC
PyInit_pyvolk(void)
{
    Py_Initialize();

    PyObject *m;

    m = PyModule_Create(&pyvolkmodule);
    import_array();
    if (m == NULL) {
        printf ("something failed\n");
        return NULL;
    }
    return m;
}
#endif